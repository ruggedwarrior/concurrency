import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import au.com.tbvp.DiningPhilosophersProblem.*;

class DiningPhilosopherTests {
	Table table;

	@BeforeEach
	void setUp() {
		table = new Table();
	}

	@Test
	void test_we_have_feast() {
		DiningPhilosophersProblem.feast();
	}

	@Test
	void test_the_table_is_set() {
		assertTrue(table.get(0 ,1));
	}

	@Test
	void test_chopsticks_get_removed() {
		table.get(0,1);
		assertFalse(table.get(0, 1));
		assertFalse(table.get(1, 2));
	}

	@Test
	void test_chopsticks_get_replaced() {
		table.get(4,0);
		assertFalse(table.get(4,0));

		table.replace(4,0);
		assertTrue(table.get(4, 0));
	}

	@Test
	void test_philosopher_arrives() {
		Philosopher socrates = new Philosopher("Socrates", 1, 0, table);
	}

	@Test
	void test_thinking_consumes_food() {
		Philosopher socrates = new Philosopher("Socrates", 1, 0, table);
		socrates.belly = 10;
		socrates.think();
		assertTrue(socrates.belly < 10); 
	}
}
