package au.com.tbvp.DiningPhilosophersProblem;

import java.util.concurrent.Callable;

public class Philosopher implements Callable<String> {
	final int FULL = 100;
	final int EMPTY = 0;
	String name;
	int[] near;
 	public int belly;
	boolean stillEating;
	Table table;

	public Philosopher(String iname, int leftChopstick, int rightChopstick, Table t) {
		name = iname;
		near = new int[2];
		near[0] = leftChopstick;
		near[1] = rightChopstick;
		belly = 0;
		table = t;
	}

	@Override
	public String call()  throws Exception{
		if (belly == EMPTY || stillEating) {
			if (getChopsticks()) {
				return eat();	
			} else {
				return "Starving";
			}
		} else {
			return think();
		}
	}

	private synchronized boolean getChopsticks() {
		if (table.get(near[0], near[1])){
			return true;
		} else {
			return false;
		}
	}

	private String eat() {
		if (belly < FULL) {
			stillEating = true;
			belly++;
		} else {
			returnChopsticks();
			stillEating = false;
		}
		return "Eating";
	}

	private void returnChopsticks() {
		table.replace(near[0], near[1]);
	}

	public String think() {
		belly--;
		return "Thinking";
	}
}
