import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

class DiningPhilosophersProblem {
	
	public static void main(String[] args) {
		int turns = 100;
		if (args.length == 1) {
			turns = Integer.parseInt(args[0]);	
		}
		System.out.println("Five philosophers, five chopsticks, and a big bowl of rice...");
		int turnsSpentHungry = 0;
		//inviteGuests();
		Philosopher heidegger = new Philosopher("Martin", 1, 2);
		Philosopher aristotle = new Philosopher("Aristotle", 2, 3);
		Philosopher plato = new Philosopher("Plato", 3, 4);
		Philosopher socrates = new Philosopher("Socrates", 4, 0);
		Philosopher deleuze = new Philosopher("Deleuze", 0, 1);
		Philosopher[] guests = new Philosopher[5];
		guests[0] = heidegger;
		guests[1] = aristotle;
		guests[2] = plato;
		guests[3] = socrates;
		guests[4] = deleuze;
		//prepareTable()
		boolean[] chopsticks = new boolean[5];
		for (int i = 0; i < chopsticks.length; i++) {
			chopsticks[i] = true;
		}
		//beginFeast()
		int j = 0;
		for (int i = 0; j < guests.length * turns; i++) {
			j++;
			turnsSpentHungry = guests[i].act(chopsticks, turnsSpentHungry);
			System.out.println(turnsSpentHungry + " philosopher's turns have been spent starving.");
			if (i == 4) { i = -1; }
		}
		System.out.println(turnsSpentHungry + " out of " + guests.length * turns + " turns were spent hungry and not eating.");

	}
}

class Philosopher {
	final int FULL = 10;
	final int EMPTY = 0;
	String name;
	int[] nearChopsticks;
	int belly;
	boolean stillEating;

	public Philosopher(String iname, int leftChopstick, int rightChopstick) {
		name = iname;
		nearChopsticks = new int[2];
		nearChopsticks[0] = leftChopstick;
		nearChopsticks[1] = rightChopstick;
		//belly = ThreadLocalRandom.current().nextInt(EMPTY, FULL+1);
		belly = 0;
	}

	public int act(boolean[] chopsticks, int turnsSpentHungry) {
		int counter = turnsSpentHungry;
		if (belly == EMPTY) {
			System.out.println(name + " is hungry...");
			counter = eat(chopsticks, turnsSpentHungry);
		} else if (stillEating) {
			counter = eat(chopsticks, turnsSpentHungry);
		
	        }else {
			System.out.println(name + " is thoughtful... (my belly is " + belly + "/10 full).");
			think();
		}
		return counter;
	}

	public int eat(boolean[] chopsticks, int turnsSpentHungry) {
		if (hasChopsticks(chopsticks)) {
			isEating(chopsticks);
		} else {
			turnsSpentHungry++;
		}	
		return turnsSpentHungry;
	}

	public boolean hasChopsticks(boolean[] chopsticks) {
		if ( chopsticks[nearChopsticks[0]] && chopsticks[nearChopsticks[1]] || stillEating) {
			chopsticks[nearChopsticks[0]] = false;
			chopsticks[nearChopsticks[1]] = false;
			stillEating = true;
			return true;
		} else {
			return false;
		}
	}

	public void isEating(boolean[] chopsticks) {
		System.out.println(name + " is eating, with chopsticks " + nearChopsticks[0] + ", and " + nearChopsticks[1] + " (my belly is " + belly + "/10 full).");
		if (belly < FULL) {
			belly++;
		} else {
			stillEating = false;
			chopsticks[nearChopsticks[0]] = true;
			chopsticks[nearChopsticks[1]] = true;
		}
	}

	public void think() {
		belly--;
	}
}
