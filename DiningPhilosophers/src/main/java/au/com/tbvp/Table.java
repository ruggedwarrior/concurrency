package au.com.tbvp.DiningPhilosophersProblem;
public class Table {
	boolean[] chopsticks;

	public Table() {
		chopsticks = new boolean[5];
		for (int i = 0; i < chopsticks.length; i++)
			chopsticks[i] = true;
	}

	public boolean get(int left, int right) {
		if (chopsticks[left] && chopsticks[right]) {
			chopsticks[left] = false;
			chopsticks[right] = false;
			return true;
		} else {
			return false;
		}
	}

	public void replace(int left, int right) {
		chopsticks[left] = true;
		chopsticks[right] = true;
	}
}
