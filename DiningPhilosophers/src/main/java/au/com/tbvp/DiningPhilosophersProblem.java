package au.com.tbvp.DiningPhilosophersProblem;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class DiningPhilosophersProblem {
	
	public static void main(String[] args) {
		feast();
	}

	public static void feast() {
		ExecutorService executor = Executors.newFixedThreadPool(5);	
		List<Future<String>> list = new ArrayList<Future<String>>();
		Table t = new Table();
		Callable<String> p1 = new Philosopher("Rousseau", 0, 1, t);
		Callable<String> p2 = new Philosopher("Paine", 1, 2, t);
		Callable<String> p3 = new Philosopher("Emerson", 2, 3, t);
		Callable<String> p4 = new Philosopher("James", 3, 4, t);
		Callable<String> p5 = new Philosopher("Dewey", 4, 0, t);
		Callable<String>[] guests = new Philosopher[5];
		guests[0] = p1;
		guests[1] = p2;
		guests[2] = p3;
		guests[3] = p4;
		guests[4] = p5;
		for (int i = 0; i < 100; i ++) {
			for (Callable<String> guest : guests) {
				Future<String> future = executor.submit(guest);
				list.add(future);
			}
		}	
		executor.shutdown();
		for (Future<String> fut : list) {
			try {
				System.out.println(fut.get());
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}
	}
}
